package CB;

public class Compte {

	private final String nom;
	private final String prenom;
	private final int numCarte;
	private double solde;
	private int credit;
	private double debit;
	
	public Compte(String nom, String prenom, int numCarte, int credit) {
		this.numCarte = numCarte;
		this.nom = nom;
		this.prenom = prenom;
		this.solde = 0;
		this.credit = credit;
		this.debit = 0;
		
		
	}
	
	public void setCredit(int credit) {
		this.credit = credit;
	}
	
	public int getNumCarte() {
		return numCarte;
	}
	public double getSolde() {
		return solde;
	}
	
	public void addSolde(double montant) {
		solde  = solde + montant;
	}
	
	public void subSolde(double montant) {
		solde  = solde - montant;
	}
	
	public void virement(Compte c, double montant){
		if(getSolde() >= montant) {
			subSolde(montant);
			c.addSolde(montant);
		}
		
	}
}
